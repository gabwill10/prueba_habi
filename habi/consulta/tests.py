from django.test import TestCase, Client

"""
I built three tests to check the response code, the content type and the json output.
"""

class GetPropertiesTestCase(TestCase):

    def test_response_code(self):
        client = Client()
        response = client.get('/get_properties/')
        self.assertEqual(response.status_code, 200)

    def test_json_output(self):
        client = Client()
        response = client.get('/get_properties/?status=foo&year=2022&city=bar')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertTrue(response.content.decode('utf-8').startswith('['))

    def test_filter_status(self):
        client = Client()
        response = client.get('/get_properties/?status=otro')
        self.assertEqual(response.status_code, 200)
        result = response.json()
        for property in result:
            self.assertNotEqual(property.get('status'), 'otro')
