SELECT  pr.address, 
        pr.city, 
        pr.price, 
        pr.description, 
        name as status_name
FROM habi_db.property pr
INNER JOIN (
	SELECT  sh.property_id, 
            sh.status_id, 
            s.name, 
            s.label, 
            sh.update_date 
	FROM habi_db.status_history sh 
	INNER JOIN (
	    SELECT  property_id, 
                MAX(update_date) AS last_update_date
	    FROM habi_db.status_history
	    GROUP BY property_id
	) lu ON lu.property_id = sh.property_id AND lu.last_update_date = sh.update_date
	INNER JOIN habi_db.status s on sh.status_id = s.id
) st ON st.property_id = pr.id 
WHERE city is not NULL 
AND address is not NULL
AND price > 0
AND status_id in (3,4,5) 
