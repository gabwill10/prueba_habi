from rest_framework import serializers

# Create a serializer class to receive the data from the request with the following fields:
# list of status of the property
# year of construction
# city
# state

class ConsultaSerializer(serializers.Serializer):
    status = serializers.CharField(
        required=False,
    )
    year = serializers.IntegerField(
        required=False,
    )
    city = serializers.CharField(
        required=False,
    )
