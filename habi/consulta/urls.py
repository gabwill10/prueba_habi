from django.urls import path
from . import views

urlpatterns = [
    path('get_properties/', views.get_properties, name='index'),
]
