import os
from django.db import connection
from django.conf import settings

def open_resources(resource: str) -> str:
	"""
	Function: open_resources

	This function allows you to open a resource file

	Parameters:
	resource (str): Name of the resource file

	Returns:
	str: Content of the resource file
	"""
	resource_file = open(os.path.join(settings.BASE_DIR,"consulta/sql/{}".format(resource)),"r")
	resource = resource_file.read()
	resource_file.close()
	
	return resource


def dictfetchall(cursor):
    """Return all rows from a cursor as a dict"""
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def execute_queries(query: str, *args):
	"""
	Function: execute_queries

	This function allows you to execute a query and return the result

	Parameters:
	query (str): Query to be executed
	*args: Arguments to be passed to the query

	Returns:
	list(dict): Result of the query
	"""
	
	with connection.cursor() as c:
		c.execute(query,*args)
		try:
			result_data = dictfetchall(c)
			c.close()
			return result_data
		except:
			c.close()
			return True