from django.db import connection
from .utils import execute_queries, open_resources


def get_properties_from_db(status: str, year: int, city: str):
    """
    Function: get_properties_from_db

    This function allows you to get the properties from the database based on the filters
    that the user has selected.

    Parameters:
    status (str): List of status of the property. Add the required states, one, none or all: ["pre_venta", "en_venta", "vendido"]
    year (int): Year of construction of the property
    city (str): City of the property

    Returns:
    list(dict): List of properties that meet the filters

    """
    
    # Open the base query: We are not using the ORM
    query = open_resources("get_properties_base.sql")

    print(status)
    if status:
        status = status.split(",")

        # status list must have at least one of the following values: "pre_venta", "en_venta", "vendido"
        if not any(s in status for s in ["pre_venta", "en_venta", "vendido"]):
            print("Status list must have at least one of the following values: 'pre_venta', 'en_venta', 'vendido'")
        else:
            status = ",".join(["'{}'".format(s) for s in status])
            query += f" AND name IN ({status})"
    
    if year:
        query += f" AND year = {year}"

    if city:
        query += f" AND city = '{city}'"

    # Execute the query and return the result
    result = execute_queries(query)

    return result