from django.shortcuts import render
from django.http import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth.decorators import permission_required
from rest_framework.permissions import IsAuthenticated
from .serializer import ConsultaSerializer
from rest_framework.views import APIView
from .db.db_implementation import get_properties_from_db



@swagger_auto_schema(
method='GET', 
operation_description=
"""
    Este endpoint permite a los usuarios consultar los inmuebles por distintos filtros:
    - Por estados de la propiedad
    - Por año de construcción
    - Por Ciudad
    - Por estado de la propiedad

    Como retorno el endpoint retornará un objeto listando todos los inmuebles que cumplan con los filtros

    ### 1. Para probar este endpoint agrege los parámetros segun su necesidad, todos son opcionales. Por defecto traerá todos los inmuebles existentes en estados de pre_venta, en_venta y vendido.
    ### 2. Haga click en el botón "Try it out" para probar el endpoint e ingresar sus parametros. 
""", 
manual_parameters=[
    openapi.Parameter(
        'status',
        openapi.IN_QUERY,
        description="""Lista de estados de la propiedad. Añada los estados requeridos, uno, ninguno o todos: [\"pre_venta\", \"en_venta\", \"vendido\"]
        Sí agrega estados diferentes no serán tomados en cuenta dentro de su consulta: Se presentará una exception en la consola del servidor.""",
        type=openapi.TYPE_ARRAY,
        items=openapi.Items(type=openapi.TYPE_STRING),
        # Add default values
        default=["pre_venta", "en_venta", "vendido"],
        required=False
    ),
    openapi.Parameter(
        'year',
        openapi.IN_QUERY,
        description="Año de construcción del inmueble",
        type=openapi.TYPE_INTEGER,
        required=False
    ),
    openapi.Parameter(
        'city',
        openapi.IN_QUERY,
        description="Ciudad del inmueble",
        type=openapi.TYPE_STRING,
        required=False
    ),
    openapi.Parameter(
        'state',
        openapi.IN_QUERY,
        description="Estado / Departamento del inmueble",
        type=openapi.TYPE_STRING,
        required=False
    ),

]
)
@api_view(['GET'])
#@permission_classes((IsAuthenticated,)) -- Activate if you are using the permission_classes decorator
#@login_required # Activate if you are using the login_required decorator 
#@permission_required('consulta.view_property', raise_exception=True) # Activate if you are using the permission_required decorator
def get_properties(request):
    """
    View: get_properties

    This view allows the user to get the properties based on the filters that the user has selected.

    Parameters:
    request (HttpRequest): Request object

    Returns:
    JsonResponse: List of properties that meet the filters
    """
    # 1. Validate the data, all query parameters are optional
    serializer = ConsultaSerializer(data=request.GET)
    serializer.is_valid(raise_exception=True)
    
    status = serializer.validated_data.get('status')
    year = serializer.validated_data.get('year')
    city = serializer.validated_data.get('city')
    
    # 2. Get the properties from the database
    result = get_properties_from_db(
        status=status,
        year=year,
        city=city,
    )
    
    # 3. Return the result
    return JsonResponse(result, safe=False)

