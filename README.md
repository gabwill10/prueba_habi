# prueba_habi
## Microservicio de consulta:
Este documento intenta explicar todas las decisiones técnicas y código que se entrega como solución a la prueba.

### Tecnologías, lenguajes y herramientas empleadas

- python:3.8
- Django
- MySQL
- Docker
- LucidChart (para diagramas)
- Poetry (para manejo de dependencias)
- Swagger (para documentación de endpoints)

### Secuencia de abordaje del proyecto

1. Lectura detallada del requerimiento.
<br>

2. Prueba de conexión con la base de datos: **Acá encontré que es una base de datos que sigue el framework de trabajo de Django, y por esta razón decidí adelantar el ejercicio usando este framework. Inicialmente pensé en usar FastAPI para resolver la prueba**
<br>

3. Desarrollo del API-Proyecto. Se incluirá como documentación de los endpoints usando Swagger.
<br>

6. Se harán los test necesarios

### Incidencias encontradas o posibles mejoras

1. La versión de mysql es la `5.7.12` y esta versión no permite el uso de la instrucción `WITH`, así que tuve que cambiar la consulta para hacerla compatible con esta versión, el resultado funciona pero es mas dificil para darle mantenimiento a la consulta posteriormente. De ser posible se debe actualizar la versión de mysql
<br>

2. La consulta de `GET /api/v1/properties/` no tiene paginación (No la incluí para el ejercicio), por lo que si se tiene una gran cantidad de registros, el tiempo de respuesta puede ser muy alto.
<br>

3. Para correr los test se debe dar permisos al usuario de la base de datos de manera que Django pueda crear la base de datos de test y no se afecten los datos de la base de producción o desarrollo.
<br>

4. Se manejaron todas las inconsistencias de la información desde la consulta de la base de datos: Definí como inconsistencias lo siguiente:
    - El precio de un inmueble debe ser superior a 0
    - La ciudad de un inmueble no puede ser vacía.
    - La dirección de un inmueble no puede ser vacía.
<br>

5. El planteamiento del problema requería poder filtrar por estado, pero en la base de datos en la tabla `habi_db.property pr` no existe un campo que permita filtrar por estado, por lo que no se incluyó en el API.

### Cómo hacer el despliegue para revisar el proyecto

1. El proyecto usa Docker y docker-compose, todo está configurado para que ejecutando docker compose arranque el servicio.

- Ejecuta el comando

    ```bash
    docker-compose up -d
    ```
- Se creará el contenedor, iniciará a instalar todas las dependencias y se levantará el servidor de Django. Esto puede tardar unos minutos.

- Una vez el contenedor arranque, por favor dirijase a la siguiente URL, esto lo llevará a la interfaz de Swagger donde podrá ver la documentación de los endpoints y probarlos. **Debe tener libre el puerto 1000 de su máquina local**

    ```bash
    http://localhost:1000
    ```

Una vez el proyecto esté levantado, puede usar los endpoints del API. y verá la siguiente interfaz (Típica de Swagger)):

![Pantalla swagger](interfaz_api.png)

**Haga click en Try it out, allí podrá ingresar todos los parámetros que requiere para ejecutar la consulta, (Todos son opcionales)**

![Pantalla swagger](parametros.png)

Los parámetros son opcionales, usted puede probar con parámetros propios, agregar o retirar dependiendo de las necesidades, di una descripción a cada uno de los parámetros para ser ingresado.

3. Para detener el proyecto, ejecute el comando:

    ```bash
    docker-compose stop
    ```
4. Para eliminar el contenedor, borrar todo, ejecute el comando:

    ```bash
    docker-compose down
    ```

### Notas:

1. No usé un ORM, lo hice con SQL.
2. Al ser solo un método GET utilicé query parameters. El front solo debe usar el endpoint. Swagger indica la URL para el testeo.
3. Las variables de la conexión a base de datos pasan al contenedor como variables de entorno, así que cada desarrollador debería tener su propio archivo .env con sus propias variables de entorno, no obstante, para efectos de la prueba y facilitar la revisión, he retirado el archivo .env del gitignore, así que el archivo .env está en el repositorio y no necesita hacer nada más.
4. No usé aseguramiento del endpoint para facilitar la prueba, pero dejé los decoradores comentados sobre el código. Como la db es de Django, podemos usar todo el esquema que ya viene para manejar roles o permisos. También sería posible usar soluciones Auth0, JWT, Cognito, etc.

## Microservicio de "Me gusta":

Para el desarrollo de este microservicio, se requiere entonces extender el modelo de datos existente para poder soportar esta información. Lineamientos del ejercicio:

1. Los usuarios pueden darle me gusta a un inmueble en específico y esto debe quedar
registrado en la base de datos.
<br>

2. Los “Me gusta” son de usuarios registrados, y debe quedar registrado en la base de
datos el histórico de “me gusta” de cada usuario y a cuáles inmuebles.

### Modelo de datos

En el archivo llamado **DiagramaDB_MeGusta.png** se encuentra el diagrama de la base de datos para almacenar esta funcionalidad. Este diagrama fue realizado con LucidChart.

![Diagrama DB](DiagramaDB_MeGusta.png)

### Explicación del modelo de datos

Para efectos del diagrama, unicamente puse las tablas que forman parte de la lógica del negocio, por eso no incluí las tablas que forman parte de Django, en el modelo aparecen dos nuevas tablas:

- **emotion_type**: Esta tabla contiene la definición de las emociones que puede registrar un usuario, para el caso podría ser "Me gusta" o "No me gusta", pero se puede extender para incluir otras emociones.
<br>

- **like_history**: Esta tabla, que bien pudiera llamarse "Emotion history", almacena todos los cambios de emociones que ha tenido un usuario en un inmueble. Por ejemplo, si un usuario le da "Me gusta" a un inmueble, y luego le da "No me gusta", se registrará en esta tabla el cambio de estado, y se mantendrá el registro de la fecha en que se hizo el cambio. También esta información puede ser útil para entender el comportamiento de las decisiones de un usuario

En el modelo se puede apreciar la relación existente contra la tabla de usuarios, que es la que mantendría la integridad referencial contra esta parte.

### Algunas mejoras para esta información.

Se podría esperar que estas tablas tengan un crecimiento exponencial muy rápido, por lo que se podría pensar en dos opciones: seguir con SQL, en cuyo caso mi recomendación es manejar tablas particionadas por fecha para poder manejar el crecimiento de la información, sin un detrimento en la velocidad de respuesta., Otra opción es pensar en una solución No-SQL, sin embargo, creo que me inclinaría por usar una solución provista por un proveedor de cloud como google y usar Google Cloud Platform para los datos que tenga una proyección de crecimiento exponencial. De esta forma podemos garantizar que la aplicación no resulte afectada con degradación del rendimiento conforme avance el tiempo.
