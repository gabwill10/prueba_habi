# Dockerfile for building a container image for the application. Ready to use.

# Use python:3.8 as base image
FROM --platform=linux/amd64 python:3.8

# Update the system
RUN apt-get update && apt-get upgrade -y

# Copy poetry installer of poetry and run it
COPY get-poetry.py /tmp/get-poetry.py

# Install poetry
RUN python /tmp/get-poetry.py

# Set the PATH
ENV PATH="/root/.local/bin:${PATH}"

# Add poetry path to bashrc
RUN echo 'export PATH="/root/.local/bin:$PATH"' >> ~/.bashrc

# Expose port 8080
EXPOSE 8080

# COPY all files from the current directory to the container
COPY . /app

# Set the working directory
WORKDIR /app

# Install dependencies
RUN poetry install

# Bash as entrypoint
CMD ["bash"]
